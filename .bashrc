#
# ~/.bashrc
#
# If not running interactively, don't do anything
#[[ $- != *i* ]] && return
# alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

jp2a Pictures/dwm.jpg

alias q='exit'
alias :q='exit'
alias c='clear'

alias r='ranger'
alias ls='ls --color=auto'
alias la='ls -a'
alias ll='ls -la'
alias l='ls'
alias l.="ls -A | egrep '^\.'"

## XBPS
alias xpf='sudo xbps-query -f '
alias xpi='sudo xbps-install '
alias xpu='sudo xbps-install -Syu'
alias xpr='sudo xbps-remove -R '
alias xpo='sudo xbps-remove -o'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#readable output
alias df='df -h'

#pacman unlock
alias unlock="sudo rm /var/lib/pacman/db.lck"
alias rmpacmanlock="sudo rm /var/lib/pacman/db.lck"

#arcolinux logout unlock
alias rmlogoutlock="sudo rm /tmp/arcologout.lock"

#free
alias free="free -mt"

#use all cores
alias uac="sh ~/.bin/main/000*"

#continue download
alias wget="wget -c"

#userlist
alias userlist="cut -d: -f1 /etc/passwd"

#merge new settings
alias merge="xrdb -merge ~/.Xresources"

# Aliases for software managment
# pacman or pm
alias pacman='sudo pacman --color auto'
alias update='sudo pacman -Syyu'

# yay as aur helper - updates everything
alias pksyua="yay -Syu --noconfirm"
alias upall="yay -Syu --noconfirm"

#ps
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#add new fonts
alias update-fc='sudo fc-cache -fv'

#copy/paste all content of /etc/skel over to home folder - backup of config created - beware
alias skel='cp -Rf ~/.config ~/.config-backup-$(date +%Y.%m.%d-%H.%M.%S) && cp -rf /etc/skel/* ~'
#backup contents of /etc/skel to hidden backup folder in home/user
alias bupskel='cp -Rf /etc/skel ~/.skel-backup-$(date +%Y.%m.%d-%H.%M.%S)'

#copy bashrc-latest over on bashrc - cb= copy bashrc
alias cb='sudo cp /etc/skel/.bashrc ~/.bashrc && source ~/.bashrc'
#copy /etc/skel/.zshrc over on ~/.zshrc - cb= copy zshrc
#alias cz='sudo cp /etc/skel/.zshrc ~/.zshrc && source ~/.zshrc'

#switch between bash and zsh
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"

#quickly kill conkies
alias kc='killall conky'

#hardware info --short
alias hw="hwinfo --short"

#skip integrity check
alias yayskip='yay -S --mflags --skipinteg'
alias trizenskip='trizen -S --skipinteg'

#check vulnerabilities microcode
alias microcode='grep . /sys/devices/system/cpu/vulnerabilities/*'

#get fastest mirrors in your neighborhood
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

#mounting the folder Public for exchange between host and guest on virtualbox
alias vbm="sudo /usr/local/bin/arcolinux-vbox-share"

#shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

#youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "


#Recent Installed Packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias riplong="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -3000 | nl"

#iso and version used to install ArcoLinux
alias iso="cat /etc/dev-rel | awk -F '=' '/ISO/ {print $2}'"

#Cleanup orphaned packages
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)'

#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"
alias piac="piactl connect"
alias piad="piactl disconnect"
alias pials="piactl monitor connectionstate"
alias piaes="sudo openvpn --mute-replay-warnings --config ~/Downloads/openvpn-strong/spain.ovpn"
alias piave="sudo openvpn --mute-replay-warnings --config ~/Downloads/openvpn-strong/venezuela.ovpn"
alias piano="sudo openvpn --mute-replay-warnings --config ~/Downloads/openvpn-strong/norway.ovpn"
alias piade="sudo openvpn --config ~/Downloads/openvpn-strong/de_frankfurt.ovpn"
#vim for important configuration files
#know what you do in these files
alias vlightdm="sudo vim /etc/lightdm/lightdm.conf"
alias vpacman="sudo vim /etc/pacman.conf"
alias vgrub="sudo vim /etc/default/grub"
alias vmkinitcpio="sudo vim /etc/mkinitcpio.conf"
alias vslim="sudo vim /etc/slim.conf"
alias voblogout="sudo vim /etc/oblogout.conf"
alias vmirrorlist="sudo vim /etc/pacman.d/mirrorlist"
alias vconfgrub="sudo vim /boot/grub/grub.cfg"
alias bls="betterlockscreen -u /usr/share/backgrounds/arcolinux/"

#gpg
#verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
#receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

#maintenance
alias big="expac -H M '%m\t%n' | sort -h | nl"
alias downgrada="downgrade --ala-url 'https://bike.seedhost.eu/arcolinux/'"

#systeminfo
alias probe="sudo -E hw-probe -all -upload"

alias exa="exa -al --color=always --group-directories-first"
alias du="ncdu --color dark -rr -x --exclude .git --exclude node_modules"
alias ksh="killall /bin/sh"
alias sdn="sudo shutdown -Ph 'now'"
alias rb="sudo reboot"
#alias a-="amixer -q -D pulse sset Master 10%-"
#alias a+="amixer -q -D pulse sset Master 10%+"
alias udm="udisksctl mount -b "
alias udu="udisksctl unlock -b "
alias udr="udisksctl unmount -b "
# Some aliases
alias s="surf -sg duck.com"
alias srls="surfraw -elfi "
alias a="sudo apt"
alias SS="sudo systemctl"
alias v="vim"
alias f="vifm"
alias pm="pulsemixer"
alias ca="castero"
alias bpy="/home/rsys/.local/bin/./bpytop"
# APT

alias zdup="sudo zypper dup"
alias zin="sudo zypper in "
alias zrm="sudo zypper rm -u "
alias ai="sudo apt install "
alias ar="sudo apt remove "
alias ari="sudo apt install --reinstall "
alias aup="sudo apt update"
alias aug="sudo apt upgrade"
alias al="apt list |grep "

alias sxiv="sxiv -ft *"
alias j11="/home/rsys/jdk-11.0.2/bin/./java "
alias e="neomutt"
alias eg="neomutt -F .gmuttrc"
alias ek="neomutt -F .kmuttrc"
alias n="newsboat"
alias mpl="mpv playlist.m3u"
alias mcam="m av://v4l2:/dev/video0 --profile=low-latency --untimed"
alias m="mpv"
alias gpe="gpg --user 4EF849BC --encrypt "
alias gpd="gpg --user 4EF849BC --decrypt "
# Adding color
alias ls='ls -hN --color=auto --group-directories-first'
alias grep="grep --color=auto"
alias diff="diff --color=auto"
alias ccat="highlight --out-format=ansi" # Color cat - print file with syntax highlighting.
alias stc="cd /home/rsys/Stack/src/bin/ && java StackMain"
# Internet
alias yt="youtube-dl --verbose --ignore-errors --restrict-filenames " # Download video link
alias yta="youtube-dl -x --audio-format vorbis -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' " # Download Album
alias ytv="youtube-dl -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' "

alias bty="echo Battery % && cat -n /sys/class/power_supply/BAT0/capacity"
alias bpytop="/home/rsys/.local/bin/./bpytop"
alias vimn="vim -c ':NotMuch'"
alias vimnm="offlineimap && notmuch new && vim -c ':NotMuch'"

alias iccc="/usr/bin/dispwin -d1 -calfile ~/Documents/VVK8Y-LQ134N1\ #1\ 2021-01-02\ 10-40\ sRGB\ M-S\ XYZLUT+MTX/VVK8Y-LQ134N1\ #1\ 2021-01-02\ 10-40\ sRGB\ M-S\ XYZLUT+MTX.cal"
alias iccl="/usr/bin/dispwin -d1 -c"
#alias xca="xcalib -d :0 -s 0 -blue 1.00 0.0 99 -green 1.00 0.0 97 ~/Documents/VVK8Y-LQ134N1sRGB.icc"
alias xca="xcalib -d :0 -s 0 -blue 1.00 0.0 99 -green 1.00 0.0 97 ~/Documents/gamma_1_0.icc"

alias xbg="xbacklight -get"
alias xbi="xbacklight -inc 5"
alias xbd="xbacklight -dec 5"

# # ex = EXtractor for all kinds of archives
# # usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

#PS1="\e[0;45;0;32m[\u@\h]\e[0;34m(\e[0m\W\e[0;34m)\e[0;135m\$ \e[0m"
#neofetch
#neofetch --title_fqdn off --package_managers tiny --os_arch off --underline on --bar_border on --backend off --gpu_brand off --disable cpu gpu --gtk2 off --gtk3 off --gtk_shorthand on --stdout
#eval "$(starship init bash)"
